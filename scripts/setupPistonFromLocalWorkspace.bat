@echo off
call setenv.bat

if "%~1"=="skipDBs" (
    set "SKIP_DBs=true"
) else (
    set "SKIP_DBs=false"
)

echo Skip DBs : %SKIP_DBs%

echo Deploying Piston ..
call ant -f ant\deployPiston.xml -DskipDBs=%SKIP_DBs%

echo Deploying admin apps ..
echo Deploying 'Apps Management' app ..
call ant -f ant\deployApp.xml -Dprefix=admin -DshortName=appsMgmt -DskipDBs=%SKIP_DBs%

echo Deploying 'Site Explorer' app ..
call ant -f ant\deployApp.xml -Dprefix=admin -DshortName=siteExplorer -DskipDBs=%SKIP_DBs%

echo Deploying 'Steam' app ..
call ant -f ant\deployApp.xml -Dprefix=admin -DshortName=steam -DskipDBs=%SKIP_DBs%

echo Deploying custom apps ..
echo Deploying 'User Management' app ..
call ant -f ant\deployApp.xml -Dprefix=custom -DshortName=userMgmt -DskipDBs=%SKIP_DBs%

echo Deploying 'Bolt' app ..
call ant -f ant\deployApp.xml -Dprefix=custom -DshortName=bolt -DskipDBs=%SKIP_DBs%

rem echo Deploying 'Turbine' app ..
rem call ant -f ant\deployApp.xml -Dprefix=custom -DshortName=turbine -DskipDBs=%SKIP_DBs%

echo Deploying custom frames ..
call ant -f ant\deployExtFrames.xml


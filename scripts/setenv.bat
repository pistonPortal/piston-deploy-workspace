@echo off

set "JAVA_HOME=c:\Program Files\Java\jdk1.8.0_92"
set "ANT_HOME=c:\setup\tools\ant-1.9.7"
set "GRADLE_HOME=c:\setup\tools\gradle-3.0"
set "CATALINA_BASE=c:\Shailendra\tomcat-profiles\piston"

set "PATH=%PATH%;%JAVA_HOME%\bin;%ANT_HOME%\bin;%GRADLE_HOME%\bin"

rem This is based on the assumption that this repository will be cloned under git-repos folder.
set "PISTON_GIT_REPOS=%cd%\..\..\.."

rem For local database
set "PISTON_DB_GIT_REPOS=%cd%\..\..\.."

rem For remote database
rem set "PISTON_DB_GIT_REPOS=/tmp/git-repos"
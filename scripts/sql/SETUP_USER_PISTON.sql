DROP USER IF EXISTS 'piston'@'%';
CREATE USER 'piston'@'%' IDENTIFIED BY 'piston';

SET @@SQL_MODE = CONCAT(@@SQL_MODE, ',NO_AUTO_CREATE_USER');
GRANT ALL PRIVILEGES ON piston.* TO 'piston'@'%';
GRANT ALL PRIVILEGES ON steam.* TO 'piston'@'%';
GRANT ALL PRIVILEGES ON userMgmt.* TO 'piston'@'%';
GRANT ALL PRIVILEGES ON bolt.* TO 'piston'@'%';
GRANT ALL PRIVILEGES ON turbine.* TO 'piston'@'%';

FLUSH PRIVILEGES;